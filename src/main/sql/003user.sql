CREATE TABLE users (
  user_id SERIAL PRIMARY KEY NOT NULL,
  nickname VARCHAR(30) UNIQUE NOT NULL,
  password CHARACTER VARYING(255) NOT NULL,
  mail CHARACTER VARYING(255)
);

CREATE TABLE mealtime (
  mealtime VARCHAR(30) PRIMARY KEY NOT NULL,
  description VARCHAR(10000)
);

CREATE TABLE user_eating (
  eating_id SERIAL PRIMARY KEY NOT NULL,
  user_id INTEGER NOT NULL,
  date_eating TIMESTAMP NOT NULL,
  mealtime VARCHAR(30),
  FOREIGN KEY (user_id) REFERENCES users(user_id),
  FOREIGN KEY (mealtime) REFERENCES mealtime(mealtime)
);

CREATE TABLE user_dish_eating (
  dish_eating_id SERIAL PRIMARY KEY NOT NULL,
  eating_id INTEGER NOT NULL,
  dish_id INTEGER NOT NULL,
  amount_dish DECIMAL NOT NULL,
  FOREIGN KEY (eating_id) REFERENCES user_eating(eating_id),
  FOREIGN KEY (dish_id) REFERENCES dish(dish_id)
);

/*
CREATE TABLE social (
  name_eng CHAR(30) PRIMARY KEY NOT NULL,
  name_rus CHAR(30) UNIQUE NOT NULL,
  attr CHAR(10) UNIQUE NOT NULL
  );

CREATE TABLE profile_social (
  social_id INTEGER NOT NULL,
  social_name_eng CHAR(30) PRIMARY KEY NOT NULL,
  social_name_rus CHAR(30) NOT NULL,
  social_attr CHAR(10) NOT NULL,
  FOREIGN KEY (social_id) REFERENCES profile(id),
  FOREIGN KEY (social_name_eng) REFERENCES social(name_eng),
  FOREIGN KEY (social_name_rus) REFERENCES social(name_rus),
  FOREIGN KEY (social_attr) REFERENCES social(attr)
);

CREATE TABLE social_page (
 id INTEGER NOT NULL,
 name CHAR(30) PRIMARY KEY NOT NULL,
 id_page VARCHAR(255) UNIQUE NOT NULL,
 FOREIGN KEY (id) REFERENCES profile_social(social_id),
 FOREIGN KEY (name) REFERENCES profile_social(social_name_eng)
);*/
