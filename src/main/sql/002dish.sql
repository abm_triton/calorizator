CREATE TABLE dish (
  dish_id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE dish_products (
  products_id SERIAL PRIMARY KEY NOT NULL,
  dish_id INTEGER NOT NULL,
  product_name VARCHAR(255) NOT NULL,
  amount_product DECIMAL NOT NULL,
  FOREIGN KEY (dish_id) REFERENCES dish(dish_id)
);