CREATE TABLE product (
  name CHARACTER VARYING(255) PRIMARY KEY NOT NULL,
  kcal INTEGER NOT NULL,
  protein NUMERIC NOT NULL,
  carbohydrate NUMERIC NOT NULL
);

CREATE TABLE product_fat (
  name CHARACTER VARYING(255) PRIMARY KEY NOT NULL,
  animal_fat DECIMAL,
  vegetable_fat DECIMAL,
  FOREIGN KEY (name) REFERENCES product(name)
);

CREATE TABLE product_vitamin (
  name VARCHAR(255) PRIMARY KEY NOT NULL,
  A1 DECIMAL,
  A2 DECIMAL,
  B1 DECIMAL,
  B2 DECIMAL,
  B3 DECIMAL,
  B4 DECIMAL,
  B5 DECIMAL,
  B6 DECIMAL,
  B7 DECIMAL,
  B8 DECIMAL,
  B9 DECIMAL,
  B10 DECIMAL,
  B11 DECIMAL,
  B12 DECIMAL,
  B13 DECIMAL,
  B15 DECIMAL,
  C DECIMAL,
  D1 DECIMAL,
  D2 DECIMAL,
  D3 DECIMAL,
  D4 DECIMAL,
  D5 DECIMAL,
  E DECIMAL,
  K1 DECIMAL,
  K2 DECIMAL,
  N DECIMAL,
  P DECIMAL,
  U DECIMAL,
  FOREIGN KEY (name) REFERENCES product(name)
);

CREATE TABLE element (
  name CHAR(30) PRIMARY KEY NOT NULL
);

CREATE TABLE product_element (
  product_name VARCHAR(255) NOT NULL,
  element_name CHAR(30) NOT NULL,
  amount_elem DECIMAL NOT NULL,
  FOREIGN KEY (product_name) REFERENCES product(name),
  FOREIGN KEY (element_name) REFERENCES element(name)
);

CREATE TABLE product_image (
  id_image SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(255) NOT NULL,
  path VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE groups (
  name CHAR(30) PRIMARY KEY NOT NULL
);

CREATE TABLE product_group (
  product_name VARCHAR(255) NOT NULL,
  group_name CHAR(30) NOT NULL,
  FOREIGN KEY (product_name) REFERENCES product(name),
  FOREIGN KEY (group_name) REFERENCES groups(name)
);

ALTER TABLE product_element ADD CONSTRAINT pk_product_element PRIMARY KEY(product_name, element_name);
ALTER TABLE product_group ADD CONSTRAINT pk_product_group PRIMARY KEY(product_name, group_name);

CREATE TABLE product_description (
  product_name VARCHAR(255) PRIMARY KEY NOT NULL,
  description VARCHAR(10000) NOT NULL,
  FOREIGN KEY (product_name) REFERENCES product(name)
);

ALTER TABLE product_description ALTER COLUMN description TYPE VARCHAR;