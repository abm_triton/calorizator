INSERT INTO mealtime (mealtime)
  VALUES ('Завтрак');

INSERT INTO mealtime (mealtime)
VALUES ('Обед');

INSERT INTO mealtime (mealtime)
VALUES ('Ужин');

INSERT INTO mealtime (mealtime)
VALUES ('breakfast');

UPDATE mealtime SET description = 'Первый дневной приём пищи, как правило — в период от рассвета до полудня[1]. Некоторые эксперты по питанию считают, что завтрак – самый важный приём пищи, его пропуск увеличивает шансы развития ожирения, сахарного диабета и может привести к сердечному приступу'
WHERE mealtime = 'Завтрак';

UPDATE mealtime SET description = 'Второй или третий приём пищи в день (обычно после первого либо второго завтрака), наиболее обильный. Как правило, на обед подаётся горячая пища. В большинстве стран обеденное время приходится на период от 12 до 15 часов, хотя официально не регламентировано, и иногда обед может происходить и вечером, заменяя собой ужин.'
WHERE mealtime = 'Обед';

UPDATE mealtime SET description = 'Последний приём пищи в конце дня, как правило, вечером или ночью. Ужин является одним из основных приёмов пищи. Также ужином называют саму пищу, приготовленную для вечернего приёма еды.'
WHERE mealtime = 'Ужин';

UPDATE mealtime SET description = 'The first day meal'
WHERE mealtime = 'breakfast';
