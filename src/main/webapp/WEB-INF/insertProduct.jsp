<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">

    <title>Добавление нового продукта</title>
</head>
<body>
<div class="container">
<jsp:include page="header.jsp" flush="true"/>

<main class="container">
    <div class="row">
        <div class="col-md-1">
        </div>

        <div class="col-md-11">
            <form class="form-horizontal" action="${pageContext.request.contextPath}/insertProductCalorizator" id="insert" method="post">
                <p>

                <h3>Добавление нового продукта</h3></p>

                <div class="form-group">
                    <div class="col-sm-10">
                        <input class="form-control" form="insert" type="text" name="productName" id="productName" placeholder="Название продукта" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <input class="form-control" form="insert" type="text" name="productKcal" id="productKcal" placeholder="Ккалории">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <input class="form-control" form="insert" type="text" name="productProtein" id="productProtein" placeholder="Белки">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <input class="form-control" form="insert" type="text" name="productCarbohydrate" id="productCarbohydrate" placeholder="Углеводы">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <input class="form-control" form="insert" type="text" name="productAFat" id="productAFat" placeholder="Животный жир">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <input class="form-control" form="insert" type="text" name="productVFat" id="productVFat" placeholder="Растительный жир">
                    </div>
                </div>

                <c:forEach var="vitamin" items="${vitaminType}">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <input class="form-control" form="insert" type="text" name="${vitamin}" placeholder="Введите витамин ${vitamin}">
                        </div>
                    </div>
                </c:forEach>

                <c:forEach var="element" items="${elementType}">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <input class="form-control" form="insert" type="text" name="${element.getElementType(element.getElement())}" placeholder="Введите элемент ${element.getElement()}">
                        </div>
                    </div>
                </c:forEach>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Добавить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-11">
            <p><a href="${pageContext.request.contextPath}/printProductsCalorizator" class="btn btn-default" role="button">Вернуться назад</a></p>
        </div>
    </div>
</main>
    </div>
</body>
</html>
