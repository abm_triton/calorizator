<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">

    <title>Добавление нового блюда</title>
</head>
<body>
<div class="container">
<jsp:include page="header.jsp" flush="true"/>
<main class="container">
    <div class="row">
        <div class="col-md-1">
        </div>

        <div class="col-md-11">
            <form class="form-horizontal" action="${pageContext.request.contextPath}/insertDish" id="insert" method="post">
                <p>
                    <h3>Добавление нового продукта</h3>
                </p>

                <div class="form-group">
                    <div class="col-sm-10">
                        <input class="form-control" form="insert" type="text" name="dishName" id="dishName" placeholder="Название блюда" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-10">
                        <input class="form-control" form="insert" type="text" name="products.key" id="productName" placeholder="Название продукта">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-10">
                        <input class="form-control" form="insert" type="text" name="products.value" id="amountProduct" placeholder="Количество продукта">
                    </div>
                </div>

                <div id="newProduct"></div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button id="buttonAddProduct" type="button" class="btn btn-default" onclick="addProduct()">Добавить продукт</button>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Добавить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-11">
            <p><a href="${pageContext.request.contextPath}/printDishes" class="btn btn-default" role="button">Вернуться назад</a></p>
        </div>
    </div>
</main>

<script>
    function addProduct() {
        var divProduct = document.createElement('div');
        divProduct.setAttribute("class", 'form-group');

        var innerDiv = document.createElement('div');
        innerDiv.setAttribute("class", 'col-sm-10');

        var inputProduct = document.createElement('input');
        inputProduct.setAttribute("class", 'form-control');
        inputProduct.setAttribute("form", 'insert');
        inputProduct.setAttribute("type", 'text');
        inputProduct.setAttribute("name", 'products.key');
        inputProduct.setAttribute("placeholder", 'Добавить новый продукт');

        innerDiv.appendChild(inputProduct);
        divProduct.appendChild(innerDiv);

        document.getElementById('newProduct').appendChild(divProduct);

        var divAmount = document.createElement('div');
        divAmount.setAttribute("class", 'form-group');

        var innerSecondDiv = document.createElement('div');
        innerSecondDiv.setAttribute("class", 'col-sm-10');

        var inputAmount = document.createElement('input');
        inputAmount.setAttribute("class", 'form-control');
        inputAmount.setAttribute("form", 'insert');
        inputAmount.setAttribute("type", 'text');
        inputAmount.setAttribute("name", 'products.value');
        inputAmount.setAttribute("placeholder", 'Количество продукта');

        innerSecondDiv.appendChild(inputAmount);
        divAmount.appendChild(innerSecondDiv);

        document.getElementById('newProduct').appendChild(divAmount);

    }

</script>
    </div>
</body>
</html>
