<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="profile" scope="request" type="com.alexmanwell.calorizator.profile.Profile"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;" charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">
    <title>Calorizator | Здравствуйте ${profile.nickname}</title>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp" flush="true"/>

    <main>
        <h1>${profile.nickname} | ${profile.mail}</h1>
        <form class="nav-header__menu-form nav-header__menu-form--create" action="${pageContext.request.contextPath}/createMealtime" id="insert" method="post">

            <fieldset>
                <legend>Создание приема пищи</legend>
                <input hidden name="nickname" value="${profile.nickname}">

                <select form="insert" size="4" multiple name="mealtime">
                    <option disabled>Выберите прием пищи</option>
                    <option selected value="breakfast">breakfast</option>
                    <option value="Завтрак">Завтрак</option>
                    <option value="Обед">Обед</option>
                    <option value="Ужин">Ужин</option>
                </select>
                <input class="field-text__input" form="insert" type="date" name="date" id="date" placeholder="Дата приема пищи">

                <input class="field-text__input" form="insert" type="time" name="time" id="time" placeholder="Время приема пищи">

                <input class="field-text__input" form="insert" type="text" name="dishes.key" id="dishName" placeholder="Название блюда" required>

                <input class="field-text__input" form="insert" type="text" name="dishes.value" id="dishAmount" placeholder="Количество блюда" required>

                <div id="newDish"></div>

                <button type="submit" class="btn btn--left btn--inline">Редактировать</button>
                <button id="addDish" type="button" class="btn btn--right btn--main btn--inline" onclick="addProduct()">Добавить блюдо</button>
            </fieldset>
        </form>

    </main>
</div>
</form>

</main>

</div>


<script>
    function addProduct() {
        var dishName = document.createElement('input');
        dishName.setAttribute("class", 'field-text__input');
        dishName.setAttribute("form", 'insert');
        dishName.setAttribute("type", 'text');
        dishName.setAttribute("name", 'dishes.key');
        dishName.setAttribute("placeholder", 'Название блюда');

        document.getElementById('newDish').appendChild(dishName);

        var dishAmount = document.createElement('input');
        dishAmount.setAttribute("class", 'field-text__input');
        dishAmount.setAttribute("form", 'insert');
        dishAmount.setAttribute("type", 'text');
        dishAmount.setAttribute("name", 'dashes.value');
        dishAmount.setAttribute("placeholder", 'Количество блюда');

        document.getElementById('newDish').appendChild(dishAmount);

    }

</script>


</body>
</html>
