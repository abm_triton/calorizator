<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">

    <title>Вывод конкретного продукта</title>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp" flush="true"/>

    <jsp:useBean id="product" scope="request" type="com.alexmanwell.calorizator.product.Product"/>

    <main>
        <c:if test="${product != null}">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 col-xxl-12">
                    <table class="table table--striped">
                        <thead class="table__header">
                        <tr>
                            <th>Название продукта</th>
                            <th>Калории</th>
                            <th>Белки</th>
                            <th>Углеводы</th>
                            <th>Животные жиры</th>
                            <th>Растительные жиры</th>
                            <th>Жиры</th>
                            <th>Группа/th>
                            <th class="grid-gradient"></th>
                        </tr>
                        </thead>
                        <tbody class="table__body">
                        <tr>
                            <td class="text-left">${product.name}</td>
                            <td class="text-right"> ${product.kcal}</td>
                            <td class="text-right"> ${product.protein}</td>
                            <td class="text-right"> ${product.carbohydrate}</td>
                            <td class="text-right"> ${product.AFat}</td>
                            <td class="text-right"> ${product.VFat}</td>
                            <td class="text-right"> ${product.fat}</td>
                            <td class="text-right"> ${product.group}</td>
                            <td class="icon">
                                <a href="${pageContext.request.contextPath}/deleteProductCalorizator?productName=${product.name}"
                                   title="удалить">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                                <a href="${pageContext.request.contextPath}/editProductCalorizator?productName=${product.name}"
                                   title="редактировать данные">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 col-xxl-12">
                    <c:if test="${product.vitamins.isEmpty() == false}">
                        <table class="vitamins table-bordered">
                            <caption class="vitaminTitle">Витамины</caption>
                            <thead>
                            <tr>
                                <c:forEach items="${product.vitamins}" var="vitamin">
                                    <c:if test="${vitamin.value != 0}">
                                        <th class="vitaminType">${vitamin.key}</th>
                                    </c:if>
                                </c:forEach>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <c:forEach items="${product.vitamins}" var="vitamin">
                                    <c:if test="${vitamin.value != 0}">
                                        <td class="vitaminValue">${vitamin.value}</td>
                                    </c:if>
                                </c:forEach>
                            </tr>
                            </tbody>
                        </table>
                    </c:if>


                    <c:if test="${product.vitamins.isEmpty() == true}">
                        <div class="row">
                            <div class="col-md-12">
                                <p><b>Витаминов в базе нету.</b></p>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 col-xxl-12">
                    <c:if test="${product.elements.isEmpty() == false}">
                        <table class="elements table-bordered">
                            <caption class="elementTitle">Элементы</caption>
                            <thead>
                            <tr>
                                <c:forEach items="${product.elements}" var="element">
                                    <c:if test="${element.value != 0}">
                                        <th class="elementType">${element.key.element}</th>
                                    </c:if>
                                </c:forEach>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <c:forEach items="${product.elements}" var="element">
                                    <c:if test="${element.value != 0}">
                                        <td class="elementValue">${element.value}</td>
                                    </c:if>
                                </c:forEach>
                            </tr>
                            </tbody>
                        </table>
                    </c:if>

                    <c:if test="${product.elements.isEmpty() == true}">
                        <div class="row">
                            <div class="col-md-12">
                                <p><b>Элементов в базе нету.</b></p>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 col-xxl-12">
                    <img src="${pageContext.request.contextPath}/imgProduct?productName=${product.name}"
                         alt="${product.name}">

                </div>
            </div>
        </c:if>


        <c:if test="${product == null}">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 col-xxl-12">
                    <p><b>Продукта ${productName} нету в базе</b></p>
                </div>
            </div>
        </c:if>

        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 col-xxl-12">
                <p><a href="${pageContext.request.contextPath}/printProductsCalorizator" class="btn btn-default"
                      role="button">Вернуться назад</a></p>
            </div>
        </div>
    </main>
</div>
</body>
</html>