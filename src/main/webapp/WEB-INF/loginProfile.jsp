<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="profile" scope="request" type="com.alexmanwell.calorizator.profile.Profile"/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">

    <title>Calorizator | Здравствуйте ${profile.nickname}</title>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp" flush="true"/>

    <aside class="">
        <section>
            <div class="user">
            </div>
            <nav>
                <ul>
                    <li>Таблица продуктов</li>
                    <li>Таблица блюд</li>
                    <li>Посмотреть</li>
                    <li>Добавить продукт</li>
                    <li>Добавить блюдо</li>
                </ul>
            </nav>
        </section>
    </aside>
    <main>
        <h1>${profile.nickname} | ${profile.mail}</h1>
        <a href="${pageContext.request.contextPath}/createMealtime?nickname=${profile.nickname}" class="btn" role="button">Создать прием пищи</a>

    </main>
    <footer>

    </footer>
    <aside>

    </aside>

</div>
</body>
</html>
