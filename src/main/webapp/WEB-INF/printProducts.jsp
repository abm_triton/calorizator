<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">

    <title>Вывод продуктов</title>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp" flush="true"/>

    <main>
        <a href="${pageContext.request.contextPath}/insertProductCalorizator" class="btn btn-default"
           role="button">Добавить новый продукт</a>


        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 col-xxl-12">
                <table class="table table--striped">
                    <caption>Спсиок всех продуктов</caption>
                    <thead class="table__header">
                    <tr>
                        <th>Название</th>
                        <th>Ккалории</th>
                        <th>Белки</th>
                        <th>Углеводы</th>
                        <th>Жиры</th>
                        <th>Группа продукта</th>
                        <th class="grid-gradient"></th>
                    </tr>
                    </thead>
                    <tbody class="table__body">
                    <c:forEach var="product" items="${products}">
                        <tr>
                            <td class="text-left"> ${product.getName()}
                            <span class="visible">
                                <a href="${pageContext.request.contextPath}/printProductCalorizator?productName=${product.getName()}">Подробнее</a>
                            </span>
                            </td>
                            <td class="text-right"> ${product.getKcal()} </td>
                            <td class="text-right"> ${product.getProtein()} </td>
                            <td class="text-right"> ${product.getCarbohydrate()} </td>
                            <td class="text-right">
                            <span class="show">${product.getFat()}
                                <em>животные: ${product.getAFat()} растительные: ${product.getVFat()}<i></i></em>
                            </span>
                            </td>
                            <td class="text-right"> ${product.getGroup()} </td>
                            <td class="icon">
                                <a href="${pageContext.request.contextPath}/deleteProductCalorizator?productName=${product.getName()}"
                                   title="удалить">
                                <span class="glyphicon glyphicon-remove">
                                </span>

                                </a>
                                <a href="${pageContext.request.contextPath}/editProductCalorizator?productName=${product.getName()}"
                                   title="редактировать данные">
                                <span class="glyphicon glyphicon-edit">
                                </span>
                                </a>
                            </td>
                        </tr>

                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </main>
</div>
</body>
</html>
