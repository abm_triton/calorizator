<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form class="fields-group--inline" id="product" method="get"
      action="${pageContext.request.contextPath}/printProductCalorizator">
    <div class="field-text__input-wrap">
        <input type="text" class="field-text__input" id="productName" form="product" name="productName" placeholder="Найти продукт" required>
    </div>
    <div class="field-text__input-wrap">
        <button type="submit" class="btn btn-default">Найти</button>
    </div>
</form>