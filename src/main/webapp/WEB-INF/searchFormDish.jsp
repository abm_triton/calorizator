<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form class="fields-group--inline" id="dish" method="get" action="${pageContext.request.contextPath}/printDish">
        <input type="text" class="field-text__input" id="dishName" form="dish" name="dishName" placeholder="Найти блюдо"
               required>
        <button type="submit" class="btn btn-default">Найти</button>
</form>
