<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">

    <title>Вывод блюда </title>
</head>
<body>
<div class="container">
<jsp:include page="header.jsp" flush="true"/>

<jsp:useBean id="dish" scope="request" type="com.alexmanwell.calorizator.dish.Dish"/>
<jsp:useBean id="result" scope="request" type="com.alexmanwell.calorizator.dish.ResultNutrientsinDish"/>

<main>
    <c:if test="${dish != null}">
        <div class="row">
            <div class="col-md-12">
                <table class="table table--striped product">
                    <thead class="table__header">
                    <tr>
                        <th>Блюдо</th>
                        <th>Ингридиенты</th>
                        <th>Кол-во</th>
                        <th class="grid-gradient"></th>
                    </tr>
                    </thead>
                    <tbody class="table__body">
                    <tr>
                        <td class="text-left">${dish.name}</td>
                        <td></td>
                        <td></td>
                        <td class="icon">
                            <a href="${pageContext.request.contextPath}/deleteDish?dishName=${dish.name}" title="удалить"><span class="glyphicon glyphicon-remove"></span></a>
                            <a href="${pageContext.request.contextPath}/editDish?dishName=${dish.name}" title="редактировать данные"><span class="glyphicon glyphicon-edit"></span></a>
                        </td>
                    </tr>
                    <c:forEach var="product" items="${dish.products}">
                        <tr>
                            <td></td>
                            <td class="text-left"> ${product.key} </td>
                            <td class="text-right"> ${product.value} </td>
                            <td></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

                <table class="table table-bordered table-striped product">
                    <thead>
                    <tr>
                        <th class="grid-gradient"></th>
                        <th>Калории</th>
                        <th>Белки</th>
                        <th>Углеводы</th>
                        <th>Животные жиры</th>
                        <th>Растительные жиры</th>
                        <th>Жиры</th>
                        <th class="grid-gradient"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-left">Итого</td>
                        <td class="text-right"> ${result.kcal}</td>
                        <td class="text-right"> ${result.protein}</td>
                        <td class="text-right"> ${result.carbohydrate}</td>
                        <td class="text-right"> ${result.aFat}</td>
                        <td class="text-right"> ${result.vFat}</td>
                        <td class="text-right"> ${result.fat}</td>
                    </tr>
                    </tbody>
                </table>

                <c:if test="${result.vitamins.isEmpty() == false}">
                    <table class="vitamins table-bordered">
                        <caption class="vitaminTitle">Витамины</caption>
                        <thead>
                        <tr>
                            <c:forEach items="${result.vitamins}" var="vitamin">
                                <c:if test="${vitamin.value != 0}">
                                    <th class="vitaminType">${vitamin.key}</th>
                                </c:if>
                            </c:forEach>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <c:forEach items="${result.vitamins}" var="vitamin">
                                <c:if test="${vitamin.value != 0}">
                                    <td class="vitaminValue">${vitamin.value}</td>
                                </c:if>
                            </c:forEach>
                        </tr>
                        </tbody>
                    </table>
                </c:if>

                <c:if test="${result.vitamins.isEmpty() == true}">
                    <div class="row">
                        <div class="col-md-12">
                            <p><b>Витаминов в базе нету.</b></p>
                        </div>
                    </div>
                </c:if>

                <c:if test="${result.elements.isEmpty() == false}">
                    <table class="elements table-bordered">
                        <caption class="elementTitle">Элементы</caption>
                        <thead>
                        <tr>
                            <c:forEach items="${result.elements}" var="element">
                                <c:if test="${element.value != 0}">
                                    <th class="elementType">${element.key.getElement()}</th>
                                </c:if>
                            </c:forEach>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <c:forEach items="${result.elements}" var="element">
                                <c:if test="${element.value != 0}">
                                    <td class="elementValue">${element.value}</td>
                                </c:if>
                            </c:forEach>
                        </tr>
                        </tbody>
                    </table>
                </c:if>

                <c:if test="${result.elements.isEmpty() == true}">
                    <div class="row">
                        <div class="col-md-12">
                            <p><b>Элементов в базе нету.</b></p>
                        </div>
                    </div>
                </c:if>

            </div>
        </div>
    </c:if>

    <c:if test="${dish == null}">
        <div class="row">
            <div class="col-md-12">
                <p><b>Блюда ${dishName} нету в базе</b></p>
            </div>
        </div>
    </c:if>

    <div class="row">
        <div class="col-md-12">
            <br/>
            <p><a href="${pageContext.request.contextPath}/printDishes" class="btn btn-default" role="button">Вернуться назад</a></p>
        </div>
    </div>

</main>
</div>
</body>
</html>
