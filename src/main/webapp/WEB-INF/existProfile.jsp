<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="profile" scope="request" type="com.alexmanwell.calorizator.profile.Profile"/>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">
    <title>Title</title>
</head>
<body>
<h1>Профайл с таким ником ${profile.nickname} уже существует</h1>
</body>
</html>
