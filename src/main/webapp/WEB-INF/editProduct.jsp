<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">
    <title>Редактирование данных</title>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp" flush="true"/>
    <main class="container">
        <div class="row">
            <div class="col-md-1">
            </div>

            <div class="col-md-11">

                <form class="form-horizontal" action="${pageContext.request.contextPath}/editProductCalorizator" id="edit" method="post">
                    <p>
                        <h3>Редактирование данных ${product.getName()}</h3>
                    </p>
                    <input hidden name="productName" value="${product.getName()}">

                    <div class="form-group">
                        <div class="col-sm-10">
                            <input class="form-control" form="edit" type="text" name="productKcal" id="productKcal" placeholder="Килокалорий">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-10">
                            <input class="form-control" form="edit" type="text" name="productProtein" id="productProtein" placeholder="Белки">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10">
                            <input class="form-control" form="edit" type="text" name="productCarbohydrate" id="productCarbohydrate" placeholder="Углеводы">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10">
                            <input class="form-control" form="edit" type="text" name="productAFat" id="productAFat" placeholder="Животный жир">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10">
                            <input class="form-control" form="edit" type="text" name="productVFat" id="productVFat" placeholder="Растительный жир">
                        </div>
                    </div>


                    <c:forEach items="${product.getVitamins()}" var="vitamin">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input class="form-control" form="edit" type="text" name="${vitamin.key}" placeholder="Витамин ${vitamin.key} = ${vitamin.value}">
                            </div>
                        </div>
                    </c:forEach>

                    <c:forEach items="${product.getElements()}" var="element">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input class="form-control" form="edit" type="text" name="${element.key}" placeholder="Элемент ${element.key.getElement()} = ${element.value}">
                            </div>
                        </div>
                    </c:forEach>


                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Редактировать</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-11">
                <a href="${pageContext.request.contextPath}/printProductsCalorizator" class="btn btn-default" role="button">Вернуться назад</a>
            </div>
        </div>
    </main>
</div>
</body>
</html>
