<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">
    <title>Редактирование данных блюда</title>
</head>
<body>
<div class="container">

    <jsp:include page="header.jsp" flush="true"/>

    <jsp:useBean id="dish" scope="request" type="com.alexmanwell.calorizator.dish.Dish"/>

    <main>
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 col-xxl-12">
                <form class="form-horizontal" action="${pageContext.request.contextPath}/editDish" id="edit" method="post">
                    <p>
                    <h3>Редактирование блюда ${dish.name}</h3></p>
                    <input hidden name="dishName" value="${dish.name}">
                    <c:forEach var="product" items="${dish.products}">
                        <div class="row">
                            <div class="field-text__input-wrap">
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3 col-xxl-3">
                                    <input class="field-text__input field-text__input--left" form="edit" type="text" name="product.key" placeholder="Редактировать продукт ${product.key}">
                                </div>
                            </div>

                            <div class="field-text__input-wrap">
                                <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3 col-xxl-3">
                                    <input class="field-text__input field-text__input--left" form="edit" type="text" name="product.value" placeholder="Количество продукта ${product.value}">
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                    <div class="row">
                        <div class="field-text__input-wrap">
                            <div class="col-sm-3 col-xs-3 col-md-3 col-lg-3 col-xxl-3">
                                <button type="submit" class="btn btn-default btn--right">Редактировать</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-11">
                <p><a href="${pageContext.request.contextPath}/printDishes" class="btn btn-default" role="button">Вернуться назад</a></p>
            </div>
        </div>
    </main>
</div>
</body>
</html>
