<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<header class="header">
    <a href="${pageContext.request.contextPath}/" class="header__logo">Calorizator</a>
    <div class="nav-header">
        <ul class="nav-header__menu">
            <li class="productsTable">
                <a href="${pageContext.request.contextPath}/printProductsCalorizator" class="btn btn-default" role="button">Вывести список всех продуктов</a>
            </li>
            <li class="dishesTable">
                <a href="${pageContext.request.contextPath}/printDishes" class="btn btn-default" role="button">Вывести список всех блюд</a>
            </li>
            <li>
                <form class="nav-header__menu-form" id="product" method="get" action="${pageContext.request.contextPath}/printProductCalorizator">
                    <input type="text" class="field-text__input" id="productName" form="product" name="productName" placeholder="Найти продукт" required>
                    <button type="submit" class="btn btn-default">Найти</button>
                </form>
            </li>
            <li>
                <label class="profile" for="window-registration">Регистрация</label>
            </li>
            <li>
                <label class="profile glyphicon glyphicon-log-in tooltip-login" data-tool="Вход" for="window-login"></label>
            </li>
        </ul>
    </div>

    <div class="window">
        <input class="window-open" id="window-login" type="checkbox" hidden>
        <div class="window-wrap" aria-hidden="true" role="dialog">
            <div class="window-dialog">
                <div class="window-header">
                    <h1>Форма входа в профиль</h1>
                    <label class="btn-close" for="window-login" aria-hidden="true">&times;</label>
                </div>
                <div class="window-body">
                    <form class="form" id="login" method="post" action="${pageContext.request.contextPath}/loginProfile">
                        <div class="field-text__input-wrap">
                            <input type="text" name="nickname" placeholder="Введите ник" class="field-text__input field-text__input--autorization" form="login" required/>
                        </div>
                        <div class="field-text__input-wrap">
                            <input type="password" name="password" placeholder="Введите пароль" class="field-text__input field-text__input--autorization" form="login" required/>
                        </div>
                        <div class="field-text__input-wrap">
                            <input name="submit" class="btn btn-default" type="submit" value="Отправить"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="window">
        <input class="window-open" id="window-registration" type="checkbox" hidden>
        <div class="window-wrap" aria-hidden="true" role="dialog">
            <div class="window-dialog">
                <div class="window-header">
                    <h1>Форма регистрации</h1>
                    <label class="btn-close" for="window-registration" aria-hidden="true">&times;</label>
                </div>
                <div class="window-body">
                    <form id="registration" method="post" action="${pageContext.request.contextPath}/createProfile">
                        <div class="field-text__input-wrap">
                            <input type="text" name="nickname" placeholder="Введите ник" class="field-text__input field-text__input--autorization" form="registration" required/>
                        </div>
                        <div class="field-text__input-wrap">
                            <input type="password" name="password" placeholder="Введите пароль" class="field-text__input field-text__input--autorization" form="registration" required/>
                        </div>
                        <div class="field-text__input-wrap">
                            <input type="password" name="confirmPassword" placeholder="Повторите пароль" class="field-text__input field-text__input--autorization" form="registration" required/>
                        </div>
                        <div class="field-text__input-wrap">
                            <input type="email" name="mail" placeholder="Введите почту" class="field-text__input field-text__input--autorization" form="registration" required/>
                        </div>
                        <div class="field-text__input-wrap">
                            <input name="submit" class="btn btn-default" type="submit" value="Зарегистрироваться"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>