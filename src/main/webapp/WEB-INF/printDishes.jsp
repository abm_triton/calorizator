<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">

    <title>Выво все блюд</title>
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp" flush="true"/>

    <main>
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 col-xxl-12">
                <jsp:include page="searchFormDish.jsp" flush="true"/>

                <a href="${pageContext.request.contextPath}/insertDish" class="btn btn-default" role="button">Добавить новое блюда</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 col-xxl-12">
                <table class="table table--striped">
                    <caption>Спсиок всех блюд</caption>
                    <thead class="table__header">
                    <tr>
                        <th>Блюдо</th>
                        <th>Ингридиенты</th>
                        <th>Кол-во</th>
                        <th class="grid-gradient"></th>
                    </tr>
                    </thead>
                    <tbody class="table__body">
                    <c:forEach var="dish" items="${dishes}">
                        <tr>
                            <td class="text-left"> ${dish.getName()}
                            <span class="visible">
                                <a href="${pageContext.request.contextPath}/printDish?dishName=${dish.getName()}">Подробнее</a>
                            </span>
                            </td>
                            <td></td>
                            <td></td>
                            <td class="icon">
                                <a href="${pageContext.request.contextPath}/deleteDish?dishName=${dish.getName()}"
                                   title="удалить">
                                <span class="glyphicon glyphicon-remove">
                                </span>
                                </a>
                                <a href="${pageContext.request.contextPath}/editDish?dishName=${dish.getName()}"
                                   title="редактировать данные">
                                <span class="glyphicon glyphicon-edit">
                                </span>
                                </a>
                            </td>
                        </tr>
                        <c:forEach var="product" items="${dish.getProducts()}">
                            <tr>
                                <td></td>
                                <td class="text-right"> ${product.key} </td>
                                <td class="text-right"> ${product.value} </td>
                                <td></td>
                            </tr>
                        </c:forEach>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </main>
</div>
</body>
</html>
