package com.alexmanwell.calorizator.product;

import java.util.Collection;
import java.util.Map;

public interface ProductDao {
    Product searchProduct(String productName) throws Exception;
    void insertProduct(Product product) throws Exception;
    void deleteProduct(String productName) throws Exception;
    Collection<Product> findAllProducts() throws Exception;
    void editProduct(Product product) throws Exception;
    Map<Product, Float> getProductsInDish(long dishId) throws Exception;
    String imgProduct(String productName) throws Exception;

}
