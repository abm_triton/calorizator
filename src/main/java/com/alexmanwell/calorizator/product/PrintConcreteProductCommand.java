package com.alexmanwell.calorizator.product;

import com.alexmanwell.calorizator.Command;
import com.alexmanwell.calorizator.CommandContext;
import com.alexmanwell.calorizator.Search;

public class PrintConcreteProductCommand implements Command {
    private String productName;

    public PrintConcreteProductCommand(String productName) {
        this.productName = productName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        Product product = context.getProductDao().searchProduct(productName);
        if (product != null) {
            Search.printTitleTable();
            Search.printProduct(product);
        } else {
            System.out.println("Продукта с таким названием нету.");
        }
    }
}
