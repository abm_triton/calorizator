package com.alexmanwell.calorizator.product;

import com.alexmanwell.calorizator.Command;
import com.alexmanwell.calorizator.CommandContext;
import com.alexmanwell.calorizator.Search;

public class PrintProductsCommand implements Command {
    @Override
    public void execute(CommandContext context) throws Exception {
        Search.printTitleTable();
        Search.printProducts(context.getProductDao().findAllProducts());
    }
}
