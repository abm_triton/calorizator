package com.alexmanwell.calorizator.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;

public class Product {

    final static Logger logger = LoggerFactory.getLogger(Product.class);

    private String name;
    private int kcal;
    private float protein;
    private float carbohydrate;
    private float aFat;
    private float vFat;
    private Map<VitaminType, Float> vitamins = new EnumMap<>(VitaminType.class);
    private Map<ElementType, Float> elements = new EnumMap<>(ElementType.class);
    private String group;
    private String description;

    private Product(Product.Builder b) {
        this.name = b.name;
        this.kcal = b.kcal;
        this.protein = b.protein;
        this.carbohydrate = b.carbohydrate;
        this.aFat = b.aFat;
        this.vFat = b.vFat;
        this.vitamins = b.vitamins;
        this.elements = b.elements;
        this.group = b.group;
        this.description = b.description;
    }

    public String getName() {
        return name;
    }

    public int getKcal() {
        return kcal;
    }

    public float getProtein() {
        return protein;
    }

    public float getCarbohydrate() {
        return carbohydrate;
    }

    public float getAFat() {
        return aFat;
    }

    public float getVFat() {
        return vFat;
    }

    public float getFat() {
        return aFat + vFat;
    }

    public String getGroup() {
        return group;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return String.format("(Название продукта: %s, килокаллории: %s, белки: %s, углеводы: %s, растительные жиры: %s, животные жиры: %s, витамины: %s, элементы: %s, группа продукта: %s)", name, kcal, protein, carbohydrate, aFat, vFat, vitamins, elements, group);
    }

    public Map<VitaminType, Float> getVitamins() {
        return vitamins;
    }

    public float getVitaminValue(VitaminType vt) {
        return vitamins.get(vt);
    }

    public Map<ElementType, Float> getElements() {
        return elements;
    }

    public float getElementValue(ElementType et) {
        if ( elements.get(et) == null) {
            return 0;
        }
        return elements.get(et);
    }

    public static class Builder {
        private String name;
        private int kcal;
        private float protein;
        private float carbohydrate;
        private float aFat;
        private float vFat;
        private Map<VitaminType, Float> vitamins = new EnumMap<>(VitaminType.class);
        private Map<ElementType, Float> elements = new EnumMap<>(ElementType.class);
        private String group;
        private String description;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setKcal(int kcal) {
            this.kcal = kcal;
            return this;
        }

        public Builder setProtein(float protein) {
            this.protein = protein;
            return this;
        }

        public Builder setCarbohydrate(float carbohydrate) {
            this.carbohydrate = carbohydrate;
            return this;
        }

        public Builder setAFat(float aFat) {
            this.aFat = aFat;
            return this;
        }

        public Builder setVFat(float vFat) {
            this.vFat = vFat;
            return this;
        }

        public Builder addVitamin(VitaminType vt, float vv) {
            this.vitamins.put(vt, vv);
            return this;
        }

        public Builder addElement(ElementType et, float ev) {
            this.elements.put(et, ev);
            return this;
        }

        public Builder setGroup(String group) {
            this.group = group;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Product build() {
            return new Product(this);
        }
    }
}
