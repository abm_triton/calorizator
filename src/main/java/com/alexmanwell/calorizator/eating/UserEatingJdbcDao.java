package com.alexmanwell.calorizator.eating;

import com.alexmanwell.calorizator.JdbcUtils;
import com.alexmanwell.calorizator.dish.Dish;
import com.alexmanwell.calorizator.profile.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;
import java.util.Date;

public class UserEatingJdbcDao implements UserEatingDao {

    final static Logger logger = LoggerFactory.getLogger(UserEatingJdbcDao.class);

    private Connection connection;

    public UserEatingJdbcDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void addEating(UserEating userEating) throws Exception {
        logger.debug("begin add dish and amount in mealtime {} {}", userEating.getMealtime(), userEating.getDate());
        try (
                PreparedStatement stmtEating = connection.prepareStatement("INSERT INTO user_eating (user_id, date_eating, mealtime) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
                PreparedStatement stmtDishEating = connection.prepareStatement("INSERT INTO user_dish_eating (eating_id, dish_id, amount_dish) VALUES (?, ?, ?)");
        ) {
            stmtEating.setLong(1, userEating.getProfileId());
            Timestamp dateEating = new Timestamp(userEating.getDate().getTime());
            stmtEating.setTimestamp(2, dateEating);
            stmtEating.setString(3, userEating.getMealtime());
            stmtEating.executeUpdate();
            ResultSet generatedKeys = stmtEating.getGeneratedKeys();

            generatedKeys.next();
            long eatingId = generatedKeys.getLong("eating_id");
            for (Map.Entry<Dish, Float> entry : userEating.getUserDish().entrySet()) {
                long dishId = entry.getKey().getId();
                float dishAmount = entry.getValue();
                stmtDishEating.setLong(1, eatingId);
                stmtDishEating.setLong(2, dishId);
                stmtDishEating.setFloat(3, dishAmount);
                stmtDishEating.executeUpdate();
            }
        }
        logger.debug("end add dish and amount in mealtime {} {}", userEating.getMealtime(), userEating.getDate());
    }

    @Override
    public void addDishEating(UserEating userEating) throws Exception {
        try (
                PreparedStatement stmtDishEating = connection.prepareStatement("INSERT INTO user_dish_eating (eating_id, dish_id, amount_dish) VALUES (?, ?, ?)");
        ) {
            for (Map.Entry<Dish, Float> entry : userEating.getUserDish().entrySet()) {
                long dishId = entry.getKey().getId();
                float dishAmount = entry.getValue();
                stmtDishEating.setLong(1, userEating.getProfileId());
                stmtDishEating.setLong(2, dishId);
                stmtDishEating.setFloat(3, dishAmount);
                stmtDishEating.getGeneratedKeys();
                stmtDishEating.executeUpdate();
            }
        }
    }

    @Override
    public void removeEating(String nickname) throws Exception {

    }

    @Override
    public void editEating(Profile profile) throws Exception {

    }

    @Override
    public UserEating searchUserEating(long profileId, Date date, String mealtime) throws Exception {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM user_eating ue LEFT JOIN user_dish_eating ude ON ue.eating_id = ude.eating_id LEFT JOIN dish d ON ude.dish_id = d.dish_id WHERE ue.user_id = ? AND ue.mealtime = ? AND ue.date_eating BETWEEN ? AND ?");
            stmt.setLong(1, profileId);
            stmt.setString(2, mealtime);
            stmt.setTimestamp(3, makeBeginTS(date));
            stmt.setTimestamp(4, makeEndTS(date));
            rs = stmt.executeQuery();

            UserEating.Builder builder = new UserEating.Builder();
            while (rs.next()) {
                builder.setProfileId(rs.getLong("user_id"));
                builder.setEatingId(rs.getLong("eating_id"));
                builder.setDateEating(rs.getTimestamp("date_eating"));
                builder.setMealtime(rs.getString("mealtime"));

                long dishId = rs.getLong("dish_id");
                String dishName = rs.getString("name");
                Dish dish = new Dish(dishId, dishName);
                float dishAmount = rs.getFloat("amount_dish");
                builder.addDish(dish, dishAmount);
            }

            UserEating userEating = builder.build();
            logger.debug("end search userEating {}", userEating);
            return userEating;
        } finally {
            JdbcUtils.closeSilently(rs);
            JdbcUtils.closeSilently(stmt);
        }
    }

    @Override
    public Set<String> mealtimes() throws Exception {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT mealtime FROM mealtime");
            rs = stmt.executeQuery();

            Set<String> set = new HashSet<>();
            while (rs.next()) {
                String mealtime = rs.getString("mealtime");
                set.add(mealtime);
            }

            return set;
        } finally {
            JdbcUtils.closeSilently(rs);
            JdbcUtils.closeSilently(stmt);
        }
    }

    @Override
    public void removeDishEating(UserEating userEating) throws Exception {
        logger.debug("begin remove dish in mealtime {}", userEating);
        PreparedStatement stmt = null;
        ResultSet rs = null;
        PreparedStatement stmtRemoveDish = null;
        PreparedStatement stmtRemoveUserEating = null;

        try {
            long idDish = 0;
            float amountDish = 0;
            for (Map.Entry<Dish, Float> entry : userEating.getUserDish().entrySet()) {
                idDish = entry.getKey().getId();
                amountDish = entry.getValue();
            }

            stmt = connection.prepareStatement("SELECT ude.eating_id FROM user_eating ue LEFT JOIN user_dish_eating ude ON ue.eating_id = ude.eating_id WHERE ue.user_id = ? AND ue.mealtime = ? AND ue.date_eating = ? AND ude.dish_id = ? AND ude.amount_dish = ?");
            stmt.setLong(1, userEating.getProfileId());
            stmt.setString(2, userEating.getMealtime());
            stmt.setTimestamp(3, new Timestamp(userEating.getDate().getTime()));
            stmt.setLong(4, idDish);
            stmt.setFloat(5, amountDish);
            logger.debug("userEating = {}, idDish = {}, amountDish = {}", userEating, idDish, amountDish);
            rs = stmt.executeQuery();

            long eatingId = 0;
            while (rs.next()) {
                eatingId = rs.getLong("eating_id");
                logger.debug("eatingId = {}", eatingId);
            }

            stmtRemoveDish = connection.prepareStatement("DELETE FROM user_dish_eating WHERE eating_id = ?");
            stmtRemoveDish.setLong(1, eatingId);
            stmtRemoveDish.executeUpdate();
            logger.debug("remove row user_dish_eating with eatingId = {}", eatingId);

            stmtRemoveUserEating = connection.prepareStatement("DELETE FROM user_eating WHERE eating_id = ?");
            stmtRemoveUserEating.setLong(1, eatingId);
            stmtRemoveUserEating.executeUpdate();
            logger.debug("remove row user_eating with eatingId = {}", eatingId);

        } finally {
            JdbcUtils.closeSilently(rs);
            JdbcUtils.closeSilently(stmt);
            JdbcUtils.closeSilently(stmtRemoveDish);
            JdbcUtils.closeSilently(stmtRemoveUserEating);
        }
        logger.debug("end remove dish in mealtime {}", userEating);
    }

    @Override
    public UserEating createMealtime(UserEating ue) throws Exception {
        logger.debug("begin insert in user_eating table new data = {}", ue);
        try (
                PreparedStatement stmtEating = connection.prepareStatement("INSERT INTO user_eating (user_id, date_eating, mealtime) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
        ) {
            stmtEating.setLong(1, ue.getProfileId());
            stmtEating.setTimestamp(2, new Timestamp(ue.getDate().getTime()));
            stmtEating.setString(3, ue.getMealtime());
            stmtEating.executeUpdate();

            ResultSet generateKeys = stmtEating.getGeneratedKeys();
            generateKeys.next();
            long eatingId = generateKeys.getLong("eating_id");

            JdbcUtils.closeSilently(generateKeys);

            UserEating.Builder b = new UserEating.Builder(ue);
            b.setEatingId(eatingId);
            UserEating newUE = b.build();
            logger.debug("end insert in user_eating table new data = {}", newUE);
            return newUE;
        }
    }

    private Timestamp makeBeginTS(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date begin = calendar.getTime();
        return new Timestamp(begin.getTime());
    }

    private Timestamp makeEndTS(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DATE, 1);

        Date end = calendar.getTime();
        return new Timestamp(end.getTime());
    }
}
