package com.alexmanwell.calorizator.eating;

import com.alexmanwell.calorizator.Command;
import com.alexmanwell.calorizator.CommandContext;

public class EndAddDishUserEating implements Command {
    @Override
    public void execute(CommandContext context) throws Exception {
        context.setUserEating(null);
    }
}
