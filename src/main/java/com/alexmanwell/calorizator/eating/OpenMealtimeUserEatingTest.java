package com.alexmanwell.calorizator.eating;

import com.alexmanwell.calorizator.CommandContext;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class OpenMealtimeUserEatingTest {

    private CommandContext context;

    @Before
    public void setUp(){
        UserEatingDao userEatingDao = new UserEatingInMemoryDao();
        context = new CommandContext(null, null, null, userEatingDao);
    }

    @Test
    public void parseMealtimeValid() throws Exception {
        String input = "Завтрак";

        OpenMealtimeUserEating eating = new OpenMealtimeUserEating(input);
        String mealtime = ParseUtils.parseMealtime(input, context);

        assertEquals("Завтрак", mealtime);
    }

    @Test
    public void parseMealtimeDateValid() throws Exception {
        String input = "Завтрак 29062016";

        OpenMealtimeUserEating eating = new OpenMealtimeUserEating(input);
        String mealtime = ParseUtils.parseMealtime(input, context);

        assertEquals("Завтрак", mealtime);
        String date = input.replace(mealtime, "").trim();

        Date actual = ParseUtils.parseDate(date);
        Date expected = new SimpleDateFormat("ddMMyyyy").parse(date);
        assertEquals(expected, actual);
    }

    @Test
    public void parseMealtimeDateInvalid() throws Exception {
        String input = "29062016 Завтрак";

        OpenMealtimeUserEating eating = new OpenMealtimeUserEating(input);
        String mealtime = ParseUtils.parseMealtime(input, context);

        assertEquals("Завтрак", mealtime);

        String date = input.replace(mealtime, "").trim();
        Date actual = ParseUtils.parseDate(date);
        Date expected = new SimpleDateFormat("ddMMyy").parse(date);
        assertEquals(expected, actual);
    }
}