package com.alexmanwell.calorizator.eating;

import com.alexmanwell.calorizator.profile.Profile;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

public interface UserEatingDao {
    void addEating(UserEating userEating) throws Exception;
    void addDishEating(UserEating userEating) throws Exception;

    void removeEating(String nickname) throws Exception;
    void editEating(Profile profile) throws Exception;
    UserEating searchUserEating(long profile, Date date, String mealtime) throws Exception;

    Set<String> mealtimes() throws Exception;

    void removeDishEating(UserEating userEating) throws Exception;

    UserEating createMealtime(UserEating ue) throws Exception;
}
