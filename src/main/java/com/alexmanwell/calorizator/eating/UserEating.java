package com.alexmanwell.calorizator.eating;

import com.alexmanwell.calorizator.dish.Dish;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class UserEating {

    private long profileId;
    private long eatingId;
    private Date date;
    private String mealtime;
    private Map<Dish, Float> userDish;

    public UserEating( UserEating.Builder ueb) {
        this.profileId = ueb.profileId;
        this.eatingId = ueb.eatingId;
        this.date = ueb.dateEating;
        this.mealtime = ueb.mealtime;
        this.userDish = ueb.userDishEating;
    }

    public UserEating(long profileId, Date date, String mealtime, Map<Dish, Float> userDish) {
        this.profileId = profileId;
        this.date = date;
        this.mealtime = mealtime;
        this.userDish = userDish;
    }

    public UserEating(long profileId, String mealtime, Map<Dish, Float> userDish) {
        this.profileId = profileId;
        this.mealtime = mealtime;
        this.userDish = userDish;
    }

    public long getProfileId() {
        return profileId;
    }

    public Date getDate() {
        return date;
    }

    public String getMealtime() {
        return mealtime;
    }

    public Map<Dish, Float> getUserDish() {
        return userDish;
    }

    @Override
    public String toString() {
        return String.format("(profileId = %d, date = %s, mealtime = %s, dishEating = %s)", profileId, date, mealtime, userDish);
    }

    public long getEatingId() {
        return eatingId;
    }

    public static class Builder {
        private long profileId;
        private long eatingId;
        private Date dateEating;
        private String mealtime;
        private Map<Dish, Float> userDishEating = new HashMap<>();

        public Builder() {}

        public Builder(UserEating ue) {
            this.profileId = ue.getProfileId();
            this.eatingId = ue.getEatingId();
            this.dateEating = ue.getDate();
            this.mealtime = ue.getMealtime();
            this.userDishEating = ue.getUserDish();
        }

        public Builder setProfileId(long profileId) {
            this.profileId = profileId;
            return this;
        }

        public Builder setEatingId(long eatingId) {
            this.eatingId = eatingId;
            return this;
        }

        public Builder setDateEating(Date dateEating) {
            this.dateEating = dateEating;
            return this;
        }

        public Builder setMealtime(String mealtime) {
            this.mealtime = mealtime;
            return this;
        }

        public Builder addDish(Dish dish, float amountDish) {
            this.userDishEating.put(dish, amountDish);
            return this;
        }

        public UserEating build() {
            return new UserEating(this);
        }
    }
}
