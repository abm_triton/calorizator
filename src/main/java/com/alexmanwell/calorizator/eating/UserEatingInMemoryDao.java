package com.alexmanwell.calorizator.eating;

import com.alexmanwell.calorizator.profile.Profile;

import java.util.*;

public class UserEatingInMemoryDao implements UserEatingDao {
    @Override
    public void addEating(UserEating userEating) throws Exception {

    }

    @Override
    public void addDishEating(UserEating userEating) throws Exception {

    }

    @Override
    public void removeEating(String nickname) throws Exception {

    }

    @Override
    public void editEating(Profile profile) throws Exception {

    }

    @Override
    public UserEating searchUserEating(long profile, Date date, String mealtime) throws Exception {
        return null;
    }

    @Override
    public Set<String> mealtimes() throws Exception {
        Set<String> mealtimes = new HashSet<String>();
        mealtimes.add("Завтрак");
        mealtimes.add("Обед");
        mealtimes.add("Ужин");
        return mealtimes;
    }

    @Override
    public void removeDishEating(UserEating userEating) throws Exception {

    }

    @Override
    public UserEating createMealtime(UserEating ue) throws Exception {
        return null;
    }
}
