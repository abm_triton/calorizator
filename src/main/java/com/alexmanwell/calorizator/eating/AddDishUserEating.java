package com.alexmanwell.calorizator.eating;

import com.alexmanwell.calorizator.Command;
import com.alexmanwell.calorizator.CommandContext;
import com.alexmanwell.calorizator.Search;
import com.alexmanwell.calorizator.dish.Dish;
import com.alexmanwell.calorizator.dish.DishDao;
import com.alexmanwell.calorizator.profile.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class AddDishUserEating implements Command {
    private final static Logger logger = LoggerFactory.getLogger(AddDishUserEating.class);

    // TODO Помечать final поля которые принимаются в конструкторе и дальше не меняют свое значение.
    private final String input;

    public AddDishUserEating(String input) {
        this.input = input;
    }

    @Override
    public void execute(CommandContext context) throws Exception {

        logger.debug("begin add dish and amount {}", input);
        Profile profile = context.getProfile();

        if (profile == null) {
            throw new IllegalArgumentException("Вы не залогинились.");
        }

        UserEating userEating = context.getUserEating();
        if (userEating == null) {
            System.out.println("Войдите в режим ввода блюд");
            return;
        }

        String dishName = ParseUtils.parseDishName(input);
        logger.debug("dishName = {}", dishName);
        float dishAmount = ParseUtils.parseDishAmount(input.replace(dishName, "").trim());
        logger.debug("dishAmount = {}", dishAmount);

        DishDao dishDao = context.getDishDao();
        Dish dish = dishDao.searchDish(dishName);

        UserEating.Builder b = new UserEating.Builder(userEating);
        b.addDish(dish, dishAmount);

        userEating = b.build();
        logger.debug("userEating = {}", userEating);
        UserEatingDao userEatingDao = context.getUserEatingDao();
        userEatingDao.addEating(userEating);

        Search.printUserEatingTitleTable();
        Search.printUserEating(profile, userEating);
        logger.debug("end add dish and amount {} in specified mealtime {} in {}", input, userEating.getMealtime(), userEating.getDate());
    }
}
