package com.alexmanwell.calorizator.eating;

import com.alexmanwell.calorizator.Command;
import com.alexmanwell.calorizator.CommandContext;
import com.alexmanwell.calorizator.Search;
import com.alexmanwell.calorizator.profile.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

public class PrintMealtimeUserEating implements Command {

    private final static Logger logger = LoggerFactory.getLogger(PrintMealtimeUserEating.class);

    @Override
    public void execute(CommandContext context) throws Exception {
        logger.debug("begin print");
        Profile profile = context.getProfile();

        if (profile == null) {
            throw new IllegalArgumentException("Вы не залогинились.");
        }

        UserEating userEating = context.getUserEating();
        String mealtime = userEating.getMealtime();
        Date date = userEating.getDate();
        logger.debug("mealtime = {}, date = {}", mealtime, date);

        UserEatingDao userEatingDao = context.getUserEatingDao();
        userEating = userEatingDao.searchUserEating(profile.getId(), date, mealtime);

        if (userEating != null) {
            Search.printUserEatingTitleTable();
            Search.printUserEating(profile, userEating);
        }
        logger.debug("end print");
    }
}
