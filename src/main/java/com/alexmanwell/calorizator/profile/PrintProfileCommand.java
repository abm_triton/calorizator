package com.alexmanwell.calorizator.profile;

import com.alexmanwell.calorizator.Command;
import com.alexmanwell.calorizator.CommandContext;
import com.alexmanwell.calorizator.Search;

public class PrintProfileCommand implements Command {

    private String nickname;

    public PrintProfileCommand(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        Profile profile = context.getProfileDao().searchProfile(nickname);
        if (profile != null) {
            Search.printProfileTitleTable();
            Search.printProfile(profile);
        } else {
            System.out.println("Профиля с таким ником нету.");
        }
    }
}
