package com.alexmanwell.calorizator.profile;

import com.alexmanwell.calorizator.Command;
import com.alexmanwell.calorizator.CommandContext;

public class ExitProfileCommand implements Command {

    @Override
    public void execute(CommandContext context) throws Exception {
        context.setProfile(null);
    }
}
