package com.alexmanwell.calorizator.profile;

import com.alexmanwell.calorizator.dish.Dish;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class Profile {

    final static Logger logger = LoggerFactory.getLogger(Profile.class);

    private long id;
    private String nickname;
    private String mail;

    public Profile(long id, String nickname, String mail) {
        this.id = id;
        this.nickname = nickname;
        this.mail = mail;
    }

    public Profile(String nickname, String mail) {
        this.nickname = nickname;
        this.mail = mail;
    }

    public long getId() {
        return id;
    }

    public String getNickname() {
        return nickname;
    }

    public String getMail() {
        return mail;
    }

    @Override
    public String toString() {
        return String.format("(Профиль| id: %s, Никнейм: %s, электропочта: %s)", id, nickname, mail);
    }
}
