package com.alexmanwell.calorizator.profile;

import com.alexmanwell.calorizator.Command;
import com.alexmanwell.calorizator.CommandContext;
import com.alexmanwell.calorizator.Search;

public class PrintAllProfileCommand implements Command {
    @Override
    public void execute(CommandContext context) throws Exception {
        Search.printProfileTitleTable();
        Search.printProfiles(context.getProfileDao().searchAllProfiles());
    }
}
