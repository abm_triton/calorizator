package com.alexmanwell.calorizator;

import com.alexmanwell.calorizator.dish.DishDao;
import com.alexmanwell.calorizator.eating.UserEating;
import com.alexmanwell.calorizator.product.ProductDao;
import com.alexmanwell.calorizator.profile.Profile;
import com.alexmanwell.calorizator.profile.ProfileDao;
import com.alexmanwell.calorizator.eating.UserEatingDao;

import java.util.Collection;

public class CommandContext {

    private ProductDao productDao;
    private DishDao dishDao;
    private ProfileDao profileDao;
    private UserEatingDao userEatingDao;
    private Profile profile;
    private UserEating userEating;

    public CommandContext(ProductDao productDao, DishDao dishDao, ProfileDao profileDao, UserEatingDao userEatingDao) {
        this.dishDao = dishDao;
        this.productDao = productDao;
        this.profileDao = profileDao;
        this.userEatingDao = userEatingDao;
    }

    public ProductDao getProductDao() {
        return productDao;
    }

    public DishDao getDishDao() {
        return dishDao;
    }

    public ProfileDao getProfileDao() {
        return profileDao;
    }

    public UserEatingDao getUserEatingDao() {
        return userEatingDao;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public void setUserEating(UserEating userEating) {
        this.userEating = userEating;
    }

    public UserEating getUserEating() {
        return userEating;
    }
}
