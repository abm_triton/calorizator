package com.alexmanwell.calorizator.dish;

import com.alexmanwell.calorizator.Command;
import com.alexmanwell.calorizator.CommandContext;
import com.alexmanwell.calorizator.Search;

public class PrintAllDishCommand implements Command {
    @Override
    public void execute(CommandContext context) throws Exception {
        Search.printDishTitleTable();
        Search.printAllDish(context.getDishDao().findAllDish());
    }
}
