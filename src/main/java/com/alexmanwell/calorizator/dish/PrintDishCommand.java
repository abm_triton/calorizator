package com.alexmanwell.calorizator.dish;

import com.alexmanwell.calorizator.Command;
import com.alexmanwell.calorizator.CommandContext;
import com.alexmanwell.calorizator.Search;

public class PrintDishCommand implements Command {

    private final String dishName;

    public PrintDishCommand(String dishName) {
        this.dishName = dishName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        Dish dish = context.getDishDao().searchDish(dishName);

        if (dish == null) {
            throw new IllegalArgumentException("Блюда с таким названием нету." + dishName);
        }
        Search.printDishTitleTable();
        Search.printDish(dish);
    }
}
