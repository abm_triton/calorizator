package com.alexmanwell.calorizator.dish;

import com.alexmanwell.calorizator.product.ElementType;
import com.alexmanwell.calorizator.product.Product;
import com.alexmanwell.calorizator.product.VitaminType;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class ResultNutrientsinDish {
    private List<Product> products;
    private float kcal;
    private float protein;
    private float carbohydrate;
    private float aFat;
    private float vFat;
    private Map<VitaminType, Float> vitamins;
    private Map<ElementType, Float> elements;

    public ResultNutrientsinDish(List<Product> products) {
        this.products = products;
    }

    public float getKcal() {
        return kcal;
    }

    public float getProtein() {
        return protein;
    }

    public float getCarbohydrate() {
        return carbohydrate;
    }

    public float getaFat() {
        return aFat;
    }

    public float getvFat() {
        return vFat;
    }

    public float getFat() {
        return aFat + vFat;
    }

    public Map<VitaminType, Float> getVitamins() {
        return vitamins;
    }

    public Map<ElementType, Float> getElements() {
        return elements;
    }

    public ResultNutrientsinDish invoke() {
        kcal = 0;
        protein = 0;
        carbohydrate = 0;
        aFat = 0;
        vFat = 0;
        vitamins = new EnumMap<>(VitaminType.class);
        elements = new EnumMap<>(ElementType.class);
        for (Product product : products) {
            kcal += product.getKcal();
            protein += product.getProtein();
            carbohydrate += product.getCarbohydrate();
            aFat += product.getAFat();
            vFat += product.getVFat();

            for (Map.Entry<VitaminType, Float> vitamin : product.getVitamins().entrySet()) {
                if (vitamins.get(vitamin.getKey()) == null) {
                    vitamins.put(vitamin.getKey(), vitamin.getValue());
                } else {
                    vitamins.put(vitamin.getKey(), vitamin.getValue() + vitamins.get(vitamin.getKey()));
                }
            }

            for (Map.Entry<ElementType, Float> element : product.getElements().entrySet()) {
                if (elements.get(element.getKey()) == null) {
                    elements.put(element.getKey(), element.getValue());
                } else {
                    elements.put(element.getKey(), element.getValue() + elements.get(element.getKey()));
                }
            }
        }
        return this;
    }
}
