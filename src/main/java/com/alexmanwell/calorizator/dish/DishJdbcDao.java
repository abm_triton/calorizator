package com.alexmanwell.calorizator.dish;

import com.alexmanwell.calorizator.JdbcUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;

public class DishJdbcDao implements DishDao {
    private final static Logger logger = LoggerFactory.getLogger(DishJdbcDao.class);

    private Connection connection;

    public DishJdbcDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Dish searchDish(String dishName) throws Exception {
        logger.debug("begin search dish");
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM dish LEFT JOIN dish_products ON dish.dish_id = dish_products.dish_id WHERE name = ?");
            stmt.setString(1, dishName);
            rs = stmt.executeQuery();
            Map<String, Float> products = new HashMap<>();
            long dishId = -1;
            while (rs.next()) {
                dishId = rs.getLong("dish_id");
                String productName = rs.getString("product_name");
                float amountProduct = rs.getInt("amount_product");
                products.put(productName, amountProduct);
            }
            if ( dishId == -1 ) {
                return null;
            }
            return new Dish(dishId, dishName, products);
        } finally {
            JdbcUtils.closeSilently(rs);
            JdbcUtils.closeSilently(stmt);
            logger.debug("end search dish");
        }
    }

    @Override
    public void insertDish(Dish dish) throws Exception {
        logger.debug("begin create dish");
        PreparedStatement stmtId = null;
        ResultSet rs = null;
        try (
                PreparedStatement stmtDish = connection.prepareStatement("INSERT INTO dish (name) VALUES (?)");
                PreparedStatement stmtProducts = connection.prepareStatement("INSERT INTO dish_products (dish_id, product_name, amount_product) VALUES (?, ?, ?)");
        ) {
            stmtDish.setString(1, dish.getName());
            stmtDish.getGeneratedKeys();
            stmtDish.executeUpdate();
            stmtId = connection.prepareStatement("SELECT dish_id FROM dish WHERE name = ?");
            stmtId.setString(1, dish.getName());
            rs = stmtId.executeQuery();

            long dishId = -1;
            while (rs.next()) {
                dishId = rs.getLong("dish_id");
            }
            for (Map.Entry<String, Float> product : dish.getProducts().entrySet()) {
                stmtProducts.setLong(1, dishId);
                stmtProducts.setString(2, product.getKey());
                stmtProducts.setFloat(3, product.getValue());
                stmtProducts.getGeneratedKeys();
                stmtProducts.executeUpdate();
            }
        } finally {
            JdbcUtils.closeSilently(rs);
            JdbcUtils.closeSilently(stmtId);
        }
        logger.debug("end create dish");
    }

    @Override
    public void deleteDish(long dishId) throws Exception {
        logger.debug("begin delete dish in DB");
        try (
                PreparedStatement stmtDish = connection.prepareStatement("DELETE FROM dish WHERE dish_id = ?");
                PreparedStatement stmtProducts = connection.prepareStatement("DELETE FROM dish_products WHERE dish_id = ?");
        ) {
            stmtProducts.setLong(1, dishId);
            stmtProducts.executeUpdate();

            stmtDish.setLong(1, dishId);
            stmtDish.executeUpdate();
        }
        logger.debug("end delete dish in DB");
    }

    @Override
    public void editDish(Dish dish, long dishId) throws Exception{
        logger.debug("begin edit dish in BD");
        try (
                PreparedStatement stmtProducts = connection.prepareStatement("UPDATE dish_products SET amount_product = ? WHERE dish_id = ? AND product_name = ?");
        ) {
            for (Map.Entry<String, Float> product : dish.getProducts().entrySet()) {
                stmtProducts.setFloat(1, product.getValue());
                stmtProducts.setLong(2, dishId);
                stmtProducts.setString(3, product.getKey());
                stmtProducts.executeUpdate();
            }
        }
    }

    @Override
    public Collection<Dish> findAllDish() throws Exception {
        logger.debug("begin find all dish");
        Map<String, Map<String, Float>> mapDish = new HashMap<>();
        try (
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM dish LEFT JOIN dish_products ON dish.dish_id = dish_products.dish_id")
        ){
            Map<String, Float> products;
            while(rs.next()) {
                String name = rs.getString("name").trim();
                products = mapDish.get(name);
                logger.debug("products = {}", products);
                if (products == null) {
                    products = new HashMap<>();
                    mapDish.put(name, products);
                }
                String productName = rs.getString("product_name");
                float amountProduct = rs.getInt("amount_product");
                products.put(productName, amountProduct);
            }
        }
        Collection<Dish> dishes = new ArrayList<>();
        for (Map.Entry<String, Map<String, Float>> pair : mapDish.entrySet()) {
            dishes.add(new Dish(pair.getKey(), pair.getValue()));
        }
        logger.debug("end find all dish {}.", dishes);
        return dishes;
    }

    @Override
    public void deleteProductDish(long dishId, List<String> products) throws Exception {
        try (
                PreparedStatement stmtDish = connection.prepareStatement("DELETE FROM dish WHERE dish_id = ?");
                PreparedStatement stmtProducts = connection.prepareStatement("DELETE FROM dish_products WHERE dish_id = ? AND product_name = ?");
        ) {
            stmtDish.setLong(1, dishId);

            for (String product : products) {
                stmtProducts.setLong(1, dishId);
                stmtProducts.setString(2, product);
                stmtProducts.executeUpdate();
            }
        }
    }

    @Override
    public void addIngridientDish(long dishId, String dishName, Map<String, Float> products) throws Exception {
        logger.debug("begin add product in dish");
        try (
                PreparedStatement stmtProducts = connection.prepareStatement("INSERT INTO dish_products (dish_id, product_name, amount_product) VALUES (?, ?, ?)");
        ) {
            for (Map.Entry<String, Float> product : products.entrySet()) {
                stmtProducts.setLong(1, dishId);
                stmtProducts.setString(2, product.getKey());
                stmtProducts.setFloat(3, product.getValue());
                stmtProducts.executeUpdate();
            }
        }
        logger.debug("end add product in dish");
    }
}
