package com.alexmanwell.calorizator.dish;

import java.util.HashMap;
import java.util.Map;

public class Dish {

    private long id;
    private String name;
    private Map<String, Float> products = new HashMap<>();

    public Dish(long id, String name, Map<String, Float> products) {
        this.id = id;
        this.name = name;
        this.products = products;
    }

    public Dish(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Dish(String name, Map<String, Float> products) {
        this.name = name;
        this.products = products;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Map<String, Float> getProducts() {
        return products;
    }


    @Override
    public String toString() {
        return String.format("(dishId = %d, name = %s, products = %s)", id, name, products);
    }
}
