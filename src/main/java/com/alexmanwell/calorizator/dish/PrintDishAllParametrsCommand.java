package com.alexmanwell.calorizator.dish;

import com.alexmanwell.calorizator.Command;
import com.alexmanwell.calorizator.CommandContext;
import com.alexmanwell.calorizator.Search;
import com.alexmanwell.calorizator.product.ElementType;
import com.alexmanwell.calorizator.product.Product;
import com.alexmanwell.calorizator.product.VitaminType;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

public class PrintDishAllParametrsCommand implements Command {

    private String dishName;

    public PrintDishAllParametrsCommand(String dishName) {
        this.dishName = dishName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        Dish dish = context.getDishDao().searchDish(dishName);
        if (dish == null) {
            throw new IllegalArgumentException("Нету блюда " + dishName);
        }

        Search.printDishTitleTable();
        Search.printDish(dish);

        Map<Product, Float> productFloatMap = context.getProductDao().getProductsInDish(dish.getId());

        List<Product> productList = getSumProductsInDish(productFloatMap);

        for (Product product : productList) {
            Search.printProduct(product);
        }
        Search.printDishTotalBJU(productList);
    }

    public static List<Product> getSumProductsInDish(Map<Product, Float> productFloatMap) {
        MathContext mc = new MathContext(3);
        List<Product> productList = new ArrayList<>();
        Search.printTitleTable();
        for (Map.Entry<Product, Float> pr : productFloatMap.entrySet()) {
            float multiplier = pr.getValue() / 100;

            Product product = pr.getKey();

            Product.Builder pb = new Product.Builder();
            pb.setName(product.getName());
            pb.setKcal(Math.round(product.getKcal() * multiplier));
            pb.setProtein(Math.round(product.getProtein() * multiplier));
            pb.setCarbohydrate(Math.round(product.getCarbohydrate() * multiplier));
            pb.setAFat(Math.round(product.getAFat() * multiplier));
            pb.setVFat(Math.round(product.getVFat() * multiplier));
            for (Map.Entry<VitaminType, Float> vitamin : product.getVitamins().entrySet()) {
                VitaminType vt = vitamin.getKey();
                BigDecimal vv = new BigDecimal(vitamin.getValue() * multiplier, mc);
                pb.addVitamin(vt, Float.parseFloat(String.valueOf(vv)));
            }

            for (Map.Entry<ElementType, Float> element : product.getElements().entrySet()) {
                ElementType et = element.getKey();
                BigDecimal ev = new BigDecimal(element.getValue() * multiplier, mc);
                pb.addElement(et, Float.parseFloat(String.valueOf(ev)));
            }
            product = pb.build();

            productList.add(product);
        }
        return productList;
    }
}
