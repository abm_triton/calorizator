package com.alexmanwell.calorizator;

public interface Command {
    void execute(CommandContext context) throws Exception;
}
