package com.alexmanwell.servlet.dish;

import com.alexmanwell.calorizator.dish.Dish;
import com.alexmanwell.calorizator.dish.DishDao;
import com.alexmanwell.calorizator.dish.PrintDishAllParametrsCommand;
import com.alexmanwell.calorizator.dish.ResultNutrientsinDish;
import com.alexmanwell.calorizator.product.Product;
import com.alexmanwell.calorizator.product.ProductDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class PrintDishServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(PrintDishServlet.class);

    private DishDao dishDao = null;
    private ProductDao productDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            dishDao = (DishDao) context.getAttribute("dishDao");
            productDao = (ProductDao) context.getAttribute("productDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        dishDao = null;
        productDao = null;
    }


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin print dish page");
        String dishName = request.getParameter("dishName");
        try {
            Dish dish = dishDao.searchDish(dishName);

            Map<Product, Float> productFloatMap = productDao.getProductsInDish(dish.getId());
            List<Product> productList = PrintDishAllParametrsCommand.getSumProductsInDish(productFloatMap);
            ResultNutrientsinDish result = new ResultNutrientsinDish(productList);
            result.invoke();

            logger.info("productFloatMap = {}", productFloatMap);
            logger.info("productList = {}", productList);
            logger.info("result = {}", result);

            request.setAttribute("dishName", dishName);

            if (dish != null && productList != null) {
                request.setAttribute("dish", dish);
                request.setAttribute("productList", productList);
                request.setAttribute("result", result);
            }
            request.getRequestDispatcher("/WEB-INF/printDish.jsp").forward(request, response);
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
        logger.info("end print dish page");
    }
}
