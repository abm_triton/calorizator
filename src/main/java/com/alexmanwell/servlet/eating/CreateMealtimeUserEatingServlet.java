package com.alexmanwell.servlet.eating;

import com.alexmanwell.calorizator.dish.Dish;
import com.alexmanwell.calorizator.dish.DishDao;
import com.alexmanwell.calorizator.eating.ParseUtils;
import com.alexmanwell.calorizator.eating.UserEating;
import com.alexmanwell.calorizator.eating.UserEatingDao;
import com.alexmanwell.calorizator.profile.Profile;
import com.alexmanwell.calorizator.profile.ProfileDao;
import com.alexmanwell.servlet.profile.LoginProfileServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CreateMealtimeUserEatingServlet extends HttpServlet {
    private final static Logger logger = LoggerFactory.getLogger(CreateMealtimeUserEatingServlet.class);

    private ProfileDao profileDao = null;
    private DishDao dishDao = null;
    private UserEatingDao userEatingDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            profileDao = (ProfileDao) context.getAttribute("profileDao");
            dishDao = (DishDao) context.getAttribute("dishDao");
            userEatingDao = (UserEatingDao) context.getAttribute("userEatingDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("begin reidrect page");

        Map<String, Float> dishes = new HashMap<>();
        request.setAttribute("dishes", dishes);

        String nickname = request.getParameter("nickname");
        try {
            Profile profile = profileDao.searchProfile(nickname);
            request.setAttribute("profile", profile);
            request.getRequestDispatcher("/WEB-INF/createMealtime.jsp").forward(request, response);
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }

        logger.debug("end reidrect page");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin create mealtime");
        String nickname = request.getParameter("nickname");
        String date = request.getParameter("date");
        String time = request.getParameter("time");
        String mealtime = request.getParameter("mealtime");

        String[] dishName = request.getParameterValues("dishes.key");
        String[] dishAmount = request.getParameterValues("dishes.value");
        logger.debug("dishAmount = {}", dishAmount);
        UserEating userEating = null;
        try {
            Profile profile = profileDao.searchProfile(nickname);
            UserEating.Builder b = new UserEating.Builder();
            b.setProfileId(profile.getId());
            b.setDateEating(ParseUtils.parseDate(date));
            b.setMealtime(mealtime);
            for (int i = 0; i < dishName.length; i++) {
                Dish dish = dishDao.searchDish(dishName[i]);
                b.addDish(dish, Float.parseFloat(dishAmount[i]));
            }
            userEating = b.build();
            userEatingDao.addEating(userEating);

            request.getRequestDispatcher("/WEB-INF/createMealtime.jsp").forward(request, response);
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
        logger.info("end create mealtime {}.", userEating);
    }

}
