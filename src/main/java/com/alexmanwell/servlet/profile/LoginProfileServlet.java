package com.alexmanwell.servlet.profile;

import com.alexmanwell.calorizator.profile.CryptPassword;
import com.alexmanwell.calorizator.profile.Profile;
import com.alexmanwell.calorizator.profile.ProfileDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginProfileServlet extends HttpServlet {

    private final static Logger logger = LoggerFactory.getLogger(LoginProfileServlet.class);

    private ProfileDao profileDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            profileDao = (ProfileDao) context.getAttribute("profileDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nickname = request.getParameter("nickname");
        String password = CryptPassword.cryptPassword(request.getParameter("password"));
        logger.debug("Nickname: {}", nickname);
        try {
            Profile profile = profileDao.loginProfile(nickname, password);
            if ( profile != null) {
                request.setAttribute("profile", profile);
            } else {
                request.getRequestDispatcher("/WEB-INF/errorPage404.jsp").forward(request, response);
            }
            request.getRequestDispatcher("/WEB-INF/loginProfile.jsp").forward(request, response);
        } catch (Exception e) {
            logger.warn("Failed to login user: {}", nickname, e);
        }
    }

}
