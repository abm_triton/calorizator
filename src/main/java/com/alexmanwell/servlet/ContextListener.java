package com.alexmanwell.servlet;

import com.alexmanwell.calorizator.ConnectionFactory;
import com.alexmanwell.calorizator.dish.DishDao;
import com.alexmanwell.calorizator.dish.DishJdbcDao;
import com.alexmanwell.calorizator.eating.UserEating;
import com.alexmanwell.calorizator.eating.UserEatingDao;
import com.alexmanwell.calorizator.eating.UserEatingJdbcDao;
import com.alexmanwell.calorizator.product.ProductDao;
import com.alexmanwell.calorizator.product.ProductJdbcDao;
import com.alexmanwell.calorizator.profile.ProfileDao;
import com.alexmanwell.calorizator.profile.ProfileJdbcDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import java.sql.SQLException;

public class ContextListener implements ServletContextListener {
    final static Logger logger = LoggerFactory.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ConnectionFactory connection = new ConnectionFactory();
        try {
            ProductDao productDao = new ProductJdbcDao(ConnectionFactory.getConnection());
            DishDao dishDao = new DishJdbcDao(ConnectionFactory.getConnection());
            ProfileDao profileDao = new ProfileJdbcDao(ConnectionFactory.getConnection());
            UserEatingDao userEatingDao = new UserEatingJdbcDao(ConnectionFactory.getConnection());

            ServletContext context = sce.getServletContext();
            context.setAttribute("productDao", productDao);
            context.setAttribute("dishDao", dishDao);
            context.setAttribute("profileDao", profileDao);
            context.setAttribute("userEatingDao", userEatingDao);

        } catch (SQLException e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
